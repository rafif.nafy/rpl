import 'package:flutter/material.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Account'),
      ),
      body: ListView(
        padding: EdgeInsets.all(16.0),
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/profile_picture.jpg'), // Ganti dengan path gambar profil pengguna
            ),
            title: Text(
              'Nama Pengguna',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              'Deskripsi Pengguna',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            onTap: () {
              // Aksi saat pengguna men-tap profil
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Edit Profile'),
            onTap: () {
              // Aksi saat pengguna men-tap "Edit Profile"
            },
          ),
          ListTile(
            leading: Icon(Icons.lock),
            title: Text('Change Password'),
            onTap: () {
              // Aksi saat pengguna men-tap "Change Password"
            },
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('Log Out'),
            onTap: () {
              // Aksi saat pengguna men-tap "Log Out"
            },
          ),
          // Tambahkan opsi pengaturan lainnya sesuai kebutuhan
        ],
      ),
    );
  }
}
