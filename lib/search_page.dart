import 'package:flutter/material.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/bg_home.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          padding: EdgeInsets.all(8.0),
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Search...',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {
                      // Aksi saat tombol search ditekan
                    },
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              width: double.infinity,
              height: 120.0,
              decoration: BoxDecoration(
                color: Colors.blueAccent, // Warna background box
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                ),
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          'PT Hoyovirse 1',
                          style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white, // Warna teks judul
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'Jawa Tengah 1',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white, // Warna teks keterangan
                        ),
                      ),
                      Text(
                        'IDR 5.000.000 - 10.000.000/bulan 1',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white, // Warna teks deskripsi
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Aksi saat item 1 dipilih
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              width: double.infinity,
              height: 120.0,
              decoration: BoxDecoration(
                color: Colors.blueAccent, // Warna background box
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                ),
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          'PT Hoyovirse 2',
                          style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white, // Warna teks judul
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'Jawa Tengah 2',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white, // Warna teks keterangan
                        ),
                      ),
                      Text(
                        'IDR 5.000.000 - 10.000.000/bulan 2',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white, // Warna teks deskripsi
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Aksi saat item 2 dipilih
                  },
                ),
              ),
            ),
            // Tambahkan item lainnya sesuai kebutuhan
          ],
        ),
      ),
    );
  }
}
