import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rekomendasi Pekerjaan'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/bg_home.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          padding: EdgeInsets.all(8.0),
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              width: double.infinity,
              height: 150.0,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 88, 160, 219), // Warna background box
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                ),
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          'PT Hoyovirse 1',
                          style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white, // Warna teks judul
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'Jawa Tengah 1',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white, // Warna teks keterangan
                        ),
                      ),
                      Text(
                        'IDR 5.000.000 - 10.000.000/bulan 1',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white, // Warna teks deskripsi
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Aksi saat item 1 dipilih
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              width: double.infinity,
              height: 150.0,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 88, 160, 219), // Warna background box
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                ),
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          'PT Hoyovirse 2',
                          style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white, // Warna teks judul
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'Jawa Tengah 2',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white, // Warna teks keterangan
                        ),
                      ),
                      Text(
                        'IDR 5.000.000 - 10.000.000/bulan 2',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white, // Warna teks deskripsi
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Aksi saat item 2 dipilih
                  },
                ),
              ),
            ),
            // Tambahkan Container lainnya sesuai kebutuhan Anda
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              width: double.infinity,
              height: 150.0,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 88, 160, 219), // Warna background box
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                ),
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          'PT Hoyovirse 3',
                          style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white, // Warna teks judul
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'Jawa Tengah 3',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white, // Warna teks keterangan
                        ),
                      ),
                      Text(
                        'IDR 5.000.000 - 10.000.000/bulan 3',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white, // Warna teks deskripsi
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Aksi saat item 3 dipilih
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              width: double.infinity,
              height: 150.0,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 88, 160, 219), // Warna background box
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                ),
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          'PT Hoyovirse 4',
                          style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white, // Warna teks judul
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'Jawa Tengah 4',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white, // Warna teks keterangan
                        ),
                      ),
                      Text(
                        'IDR 5.000.000 - 10.000.000/bulan 4',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white, // Warna teks deskripsi
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Aksi saat item 4 dipilih
                  },
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8.0),
              width: double.infinity,
              height: 150.0,
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 88, 160, 219), // Warna background box
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                ),
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 16.0),
                        child: Text(
                          'PT Hoyovirse 5',
                          style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white, // Warna teks judul
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        'Jawa Tengah 5',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white, // Warna teks keterangan
                        ),
                      ),
                      Text(
                        'IDR 5.000.000 - 10.000.000/bulan 5',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white, // Warna teks deskripsi
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Aksi saat item 5 dipilih
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
